from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)

db = SQLAlchemy(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:admin123@localhost/author_db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

class Author_work(db.Model):
    __tablename__ = 'records'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(20), nullable=False, unique=False)
    last_name = db.Column(db.String(20), nullable=False, unique=False)
    story = db.Column(db.Text(), nullable=False, unique=False)
    submission_date = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, first_name, last_name, story):
        self.first_name = first_name
        self.last_name = last_name
        self.story = story
        

    def __repr__(self):
        return '<Story %r>' % self.id



@app.route("/")
def index():
    return render_template('index.html')

@app.route("/submit", methods=['POST'])
def submit():
    if request.method == 'POST':
        if request.form['fname'].strip() == '' or request.form['lname'].strip() == '' or request.form['story'].strip() == '':                     
            return render_template('index.html', error_message = 'Please ensure all fields are filled')

        doc = open('author_file.txt', 'w')
        content = f"""Author name : {request.form['fname'].strip()} {request.form['lname'].strip()} \nAuthor Story : {request.form['story'].strip()}"""
        doc.write(content)
        doc.close()        

        doc = open('author_file.txt', 'r')
        
        for line in doc:
            if line.startswith("Author name :"):
                names = line.replace("Author name : ", "").split()
                first_name = names[0]
                last_name = names[1]
                
            else:
                story = line.replace("Author Story : ", "")           


        doc.close()

              
        data = Author_work(first_name, last_name, story)
        db.session.add(data)
        db.session.commit()    

        return render_template('success.html')


if __name__ == "__main__":
    app.run(debug=True)


